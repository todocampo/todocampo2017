
(function() {


    'use strict';
    angular.module('form', [ ])

        .directive('loading', ['$http', function($http) {
            return {
                restrict: 'A',
                link: function (scope, elm) {
                    scope.isLoading = function () {
                        return $http.pendingRequests.length > 0;
                    };
                    
                    scope.$watch(scope.isLoading, function (v) {
                        if (v) {
                            console.log(elm);
                            angular.element(elm).css('display', 'block');
                        } else {
                            angular.element(elm).css('display', 'none');
                            angular.element(document.querySelector('#formulario')).css('display', 'block');

                        }
                    });
                }
            };
        }])
    






        .controller("FormCtrl", function($scope, $http) {

            $scope.submitForm = function(){
                if($scope.formulario.$valid){
                    console.log("PASO VALIDACION");

                    var DataForm = {
                                    nombre : $scope.frm.nombre,
                                    email : $scope.frm.email,
                                    fono : $scope.frm.fono,
                                    tipo : 'FORM-CONTACTO',
                                    msg : $scope.frm.msg,
                                    origen : 'BIBLIOTECA'
                                };


                    $http({
                        url: 'http://api.dgca.io/v1/email/envio',
                        method: 'post',
                        data: DataForm,
                    }).then(function (response) { 
                        $scope.frm.nombre="";
                        $scope.frm.email="";
                        $scope.frm.fono="";
                        $scope.frm.msg="Su solicitud fue enviada!";

                        for (var att in  $scope.formulario.$error) {
                            if ($scope.formulario.$error.hasOwnProperty(att)) {
                                $scope.formulario.$setValidity(att, true);
                            }
                        }

                        $scope.formulario.$error = {};
                        $scope.formulario.$dirty = false;
                        $scope.formulario.$pristine = true;
                        $scope.formulario$submitted = false;
                        angular.element(document.querySelector('#msg')).css('display', 'block');
                        $scope.submitOk = true;

                    }, function (error) { console.log("ERROR:" + error); }
                    );



                }else{
                    console.log("Error");
                }
            }

        });

}());








