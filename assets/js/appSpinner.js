 
(function() {


    'use strict';
    angular.module('form', [ ])


        .directive('ngLoading', function (Session, $compile) {

            var loadingSpinner = '<div class="spinner"><div class="dot1"></div><div class="dot2"></div></div>'; // <LOADING_ICON_HTML_HERE>;

            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    var originalContent = element.html();
                    element.html(loadingSpinner);
                    scope.$watch(attrs.ngLoading, function (val) {
                        if(val) {
                            element.html(originalContent);
                            $compile(element.contents())(scope);
                        } else {
                            element.html(loadingSpinner);
                        }
                    });
                }
            };
        });



}());


