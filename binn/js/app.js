
(function() {


    'use strict';
    angular.module('form', [ ])


        .controller("FormCtrl", function($scope, $http) {
            $scope.submitForm = function(){
                if($scope.formulario.$valid){
                    console.log("PASO VALIDACION");

                    var DataForm = {
                                    nombre : $scope.frm.nombre,
                                    email : $scope.frm.email,
                                    fono : $scope.frm.fono,
                                    tipo : 'FORM-CONTACTO',
                                    msg : $scope.frm.msg,
                                    origen : 'EVENTOS-TODO-CAMPO'
                                };


                    $http({
                        //url: ' http://login.dgca.io/api2/sesion/crear',
                        url: 'http://api.dgca.io/v1/email/envio',
                        //skipAuthorization: true,
                        method: 'post',
                        data: DataForm,
                        //withCredentials: true,
                        //headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).then(function (response) { //Si es aceptado por el servidor, realiza lo siguiente:
                        //Devuelve a la página home
                        //console.log("CONSUMIDA LA API ");
                        $scope.frm.nombre="";
                        $scope.frm.email="";
                        $scope.frm.fono="";
                        $scope.frm.msg="";

                        for (var att in  $scope.formulario.$error) {
                            if ($scope.formulario.$error.hasOwnProperty(att)) {
                                $scope.formulario.$setValidity(att, true);
                            }
                        }

                        $scope.formulario.$error = {};
                        $scope.formulario.$dirty = false;
                        $scope.formulario.$pristine = true;
                        $scope.formulario$submitted = false;

                        $scope.submitOk = true;


                    }, function (error) { //En caso contrario
                        //Presenta un mensaje de error
                        // var el = angular.element(document.getElementsByClassName("error-login"));
                        // el.text(error.data.mensaje);
                        // el.css('display', 'block');
                        console.log("ERROR:" + error);
                    });



                }else{
                    console.log("Error");
                }
            }

        });

}());








